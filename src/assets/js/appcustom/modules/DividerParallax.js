import $ from 'jquery';
import parallax from 'jquery-parallax';

class DividerParallax {
  constructor() {
    console.log("Parallax");
    this.slider = $('.parallaxDivider');
    this.effect();
  }

  events() {
    $(window).scroll(this.effect());
  }

  effect() {
    this.slider.parallax("center", 0.5, true);
  }

}
export default DividerParallax;