import $ from 'jquery';




class GalleryModal {
  constructor() {
    console.log('hello from gallery');
    this.modal = $('.gallery-modal');
    this.openModalButton = $('.open-modal');
    this.closeModalButton = $(".gallery-modal__close-button");    
    this.modalImage = $('.gallery-modal__image');
    this.events();
  }

  events() {

    var that = this;
    this.openModalButton.on('click', function(){
    this.currentImageSrc = $(this).attr('src');

    $('.gallery-modal__image').attr('src', this.currentImageSrc);
    });
     
    // clicking the open modal openModalButton
    this.openModalButton.click(this.openModal.bind(this));
    // clicking the x close modal button
    this.closeModalButton.click(this.closeModal.bind(this));
    // pushes any key
    $(document).keyup(this.keyPressHandler.bind(this));
  }

  keyPressHandler(e){
    if(e.keyCode == 27){
      this.closeModal();
    }
  }

  openModal(){
    this.modal.addClass("gallery-modal--is-visible");
    return false;
  }

  closeModal(){
    this.modal.removeClass("gallery-modal--is-visible");
  }
}

export default GalleryModal;