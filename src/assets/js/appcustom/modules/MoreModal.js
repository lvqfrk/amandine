import $ from 'jquery';

class MoreModal {
  constructor() {
    this.modal = $('.more');
    this.openModalButton = $('.intro__container__buttons__about');
    this.closeModalButton = $(".more__close-button");
    this.events();
  }

  events() {
    // clicking the open modal openModalButton
    this.openModalButton.click(this.openModal.bind(this));
    // clicking the x close modal button
    this.closeModalButton.click(this.closeModal.bind(this));
    // pushes any key
    $(document).keyup(this.keyPressHandler.bind(this));
  }

  keyPressHandler(e){
    if(e.keyCode == 27){
      this.closeModal();
    }
  }

  openModal(){
    this.modal.addClass("more--is-visible");
    return false;
  }

  closeModal(){
    this.modal.removeClass("more--is-visible");
  }
}

export default MoreModal;