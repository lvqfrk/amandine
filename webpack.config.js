var path = require('path');

module.exports = {
  entry: {
    App: "./src/assets/js/appcustom/app.js",
    //Vendor: "./app/assets/scripts/Vendor.js"
  },
  output: {
    path: path.resolve(__dirname, "./src/assets/js/temp"),
    filename: "[name].js"
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        query: {
          presets: ["@babel/preset-env"]
        },
        test: /\.js$/,
        exclude: /node_modules/
      }
    ]
  }
}
