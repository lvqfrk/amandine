<?php /* Template Name: PageGallery */ ?>
<?php get_header(); ?>
<div class="main-container container">

    <h1 class="gallery__title">Laurem Ipsum</h1>

    <div class="row gallery__content">
        <div class="col-12 g-0">
            <!-- query -->
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $query = new WP_Query(array(
                'posts_per_page' => 12,
                'paged' => $paged
            ));
            ?>

            <?php if ($query->have_posts()) : ?>

                <!-- begin loop -->
                <div class="row g-0">
                    <?php while ($query->have_posts()) : $query->the_post(); ?>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 gallery__content__item">
                            <a href="<?php the_permalink(); ?>" >
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('post-thumbnail') ?>
                                <?php endif ?>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
                <!-- end loop -->


                <div class="pagination">
                    <?php
                    echo paginate_links(array(
                        'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
                        'total'        => $query->max_num_pages,
                        'current'      => max(1, get_query_var('paged')),
                        'format'       => '?paged=%#%',
                        'show_all'     => false,
                        'type'         => 'plain',
                        'end_size'     => 2,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf('<i></i> %1$s', __('Newer Posts', 'text-domain')),
                        'next_text'    => sprintf('%1$s <i></i>', __('Older Posts', 'text-domain')),
                        'add_args'     => false,
                        'add_fragment' => '',
                    ));
                    ?>
                </div>



                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>