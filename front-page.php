<?php get_header(); ?>
<div class="more">
    <p class="more__close-button">X</p>
    <p><b>Besoin d'un graphiste pour réaliser votre identité visuelle ?</b><br><br>
        Je me mets à votre disposition pour créer votre projet,
        que vous ayez une idée précise ou non. <br><br>Je prends le temps de
        discuter avec vous, pour comprendre l'essence même de ce
        dernier, afin de vous faire une proposition au plus juste.<br>
        chaque projet est unique car vous êtes unique. <br><br>
        Je m'adapte à vos besoin avec mes qualités de mobilité et
        d'empathie, et de solide base de graphisme, en passant part la
        connaissance de la chaine graphique obtenu avec mes études.</p>
</div>
<section class="intro">
    <div class="container intro__container">
        <img class="intro__container__logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/aw_logo.png" alt="logo de AW design graphique">
        <div class="intro__container__buttons">
            <a class="intro__container__buttons__about">En savoir plus</a>
            <a href="#contact" class="intro__container__buttons__contact">Me contacter</a>
        </div>
    </div>
</section>
<section class="logo">
    <div class="container logo__container">
        <div class="row">
            <div class="col-sm-6">
                <p class="logo__container__text"><b>Besoin d'un graphiste pour réaliser votre identité visuelle ?</b><br><br>
                    Je me mets à votre disposition pour créer votre projet,
                    que vous ayez une idée précise ou non.<br>Je prends le temps de
                    discuter avec vous, pour comprendre l'essence même de ce
                    dernier, afin de vous faire une proposition au plus juste.</p>
            </div>
            <div class="col-sm-6 d-block d-sm-none">
                <img class="logo__container__exemple" src="<?php echo get_template_directory_uri(); ?>/assets/img/photo_test.png" alt="exemple d'un logo">
            </div>
        </div>
    </div>

</section>
<section class="flyer">
    <div class="container flyer__container">
        <div class="row">
            <div class="col-lg-6 order-lg-2">
                <p class="flyer__container__text"><b>Besoin d'une identité visuelle ?</b><br><br>
                    Je crée votre logo, ainsi que le design de vos cartes de visites, flyers ou autres supports.</p>
            </div>
            <div class="col-lg-6 order-lg-1">
                <img class="flyer__container__exemple" src="<?php echo get_template_directory_uri(); ?>/assets/img/exemple-logo.png" alt="exemple d'un logo">
            </div>

        </div>
    </div>
</section>
<section class="affiche">
    <div class="container affiche__container">
        <div class="row">
            <div class="col-lg-6">
                <p class="affiche__container__text"><b>Un besoin spécifique ?</b><br><br>
                    Je vous propose mes services pour le design de vos vehicules, vêtements, ou autres projets d'imprimerie. N'hésitez pas a me contacter.</p>
            </div>
            <div class="col-lg-6">
                <img class="affiche__container__exemple" src="<?php echo get_template_directory_uri(); ?>/assets/img/exemple_affiche.png" alt="exemple d'un logo">
            </div>
        </div>
    </div>
</section>
<section class="gallery">
    <div class="gallery-modal">
        <p class="gallery-modal__close-button">X</p>
        <img class="gallery-modal__image" src="" alt="gallery image">
    </div>
    <div class="container gallery__container">
        <!-- first line -->
        <div class="row g-0">
            <div class="col-6">
                <img class="gallery__container__main_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo001.jpg" alt="exemple d'un logo">
            </div>
            <div class="col-6">
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo002.jpg" alt="exemple d'un logo">
                    </div>
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo003.jpg" alt="exemple d'un logo">
                    </div>

                </div>
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo004.jpg" alt="exemple d'un logo">
                    </div>
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo005.jpg" alt="exemple d'un logo">
                    </div>
                </div>
            </div>
        </div>
        <!-- second line -->
        <div class="row g-0">
            <div class="col-3">
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo006.png" alt="exemple d'un logo">
                    </div>

                </div>
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo007.jpg" alt="exemple d'un logo">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <img class="gallery__container__main_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo008.jpg" alt="exemple d'un logo">
            </div>
            <div class="col-3">
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo009.jpg" alt="exemple d'un logo">
                    </div>


                </div>
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo010.jpg" alt="exemple d'un logo">
                    </div>
                </div>
            </div>
        </div>
        <!-- third line -->
        <div class="row g-0">

            <div class="col-6">
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo012.jpg" alt="exemple d'un logo">
                    </div>
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo011.png" alt="exemple d'un logo">
                    </div>

                </div>
                <div class="row g-0">
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo013.jpg" alt="exemple d'un logo">
                    </div>
                    <div class="col">
                        <img class="gallery__container__secondary_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo014.png" alt="exemple d'un logo">
                    </div>
                </div>
            </div>

            <div class="col-6">
                <img class="gallery__container__main_pic open-modal" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/photo015.jpg" alt="exemple d'un logo">
            </div>
        </div>
    </div>
</section>
<section class="contact" id="contact">
    <div class="container contact__container">
        <div class="row">
            <div class="col-lg-7 contact__container__info">
                <p><b>Amandine Werly</b><br>07 61 58 72 28 <br>aw.designgraphique@gmail.com <br>Siret: 90243895100011 <br>24 Créneleuc,22230 Laurenan</p><br><br>
                <p><b>Hebergeur:</b> <br>SAS OVH <br>2 rue Kellermann– 59100 Roubaix – France<br>Site : www.ovh.com</p><br><br>
            </div>
            <div class="col-lg-5 contact__container__form">
                <?php echo do_shortcode("[contact-form-7 id='6' title='Formulaire de contact 1']") ?>
                
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>