<?php
require get_template_directory() . '/bootstrap-navwalker.php';
function gwezenn_files() {
	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css2?family=Alegreya+Sans+SC&display=swap');

	wp_enqueue_style('bootstrap4', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css');
	wp_enqueue_style('gwezenn_main_styles', get_template_directory_uri() . '/assets/css/main.css');
	wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_script( 'boot1','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'boot4',get_template_directory_uri() . '/assets/js/appcustom.js', array( 'jquery' ),'',true );
}

add_action('wp_enqueue_scripts', 'gwezenn_files');

// Add menus
add_theme_support('menus');
add_theme_support('post-thumbnails');

// Register menus
register_nav_menus(

	array(
		'top-menu' => 'Top Menu',
	)
);

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


 ?>
